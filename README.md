# README #

### What is this repository for? ###

A web application based on the lines of Zomato, where users can rate and review restaurants and diners, as well as read others reviews.
Developed on C# .Net Entity Framework and MySql2 and frontend using Bootstrap.
* Version - 2.0


### How do I get set up? ###

* Summary of set up
* Configuration -- basic .Net C# initial setup
* Dependencies -- Bootstrap 
* Database configuration
* How to run tests -- N/A
* Deployment instructions -- usual .Net C# deployment procedure.

### Who do I talk to? ###

* Repo owner or admin - Rajveer Shringi
* Other community or team contact